package pitapp.com.clckablestring;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import java.util.ArrayList;

import cn.iwgang.simplifyspan.SimplifySpanBuild;
import cn.iwgang.simplifyspan.other.OnClickableSpanListener;
import cn.iwgang.simplifyspan.unit.SpecialClickableUnit;
import cn.iwgang.simplifyspan.unit.SpecialTextUnit;

/**
 * Created by Anton Pitaev on 02/09/18.
 */
public class ClickableString {
    private String word;
    private int type;

    public static final String DOES = "DOES", NOT = "NOT";
    public static final String READ = "Read", MORE = "more...";
    public static final String VITAMIN = "Vitamin", C = "C";
    public static final String YO = "YO", TEST = "TEST", RESULT = "RESULT";
    public static final String BR = "<br>";


    public static final String LINE_BREAKER = "\n";

    private static final int TYPE_NORMAL = 0,
            TYPE_DOES_NOT = 1,
            TYPE_READ_MORE = 2,
            TYPE_VITAMIN_C = 3,
            TYPE_YO_TEST_RESULT = 4,
            TYPE_LINE_BREAKER =5;

    public ClickableString(String word, int type) {
        setWord(word);
        setType(type);
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getWord() {
        return word;
    }

    private void setWord(String word) {
        this.word = word;
    }

    public static void setClickableString(String text, TextView tv, OnClickableSpanListener spanListener) {
        if (text.trim().length() < 1) return;
        ArrayList<ClickableString> clickableStringArrayList = new ArrayList<>();
        String[] words = text.split(" ");

        for (int i = 0; i < words.length; i++) {
            String word = words[i].trim();

            boolean DOES_NOT_PATTERN = word.equals(DOES) && (i + 1) < words.length && words[i + 1] != null && words[i + 1].equals(NOT) || word.equals(NOT) && (i - 1) >= 0 && words[i - 1] != null && words[i - 1].equals(DOES);
            boolean READ_MORE_PATTERN = word.equals(READ) && (i + 1) < words.length && words[i + 1] != null && words[i + 1].equals(MORE) || word.equals(MORE) && (i - 1) >= 0 && words[i - 1] != null && words[i - 1].equals(READ);
            boolean VITAMIN_C_PATTERN = word.equals(VITAMIN) && (i + 1) < words.length && words[i + 1] != null && words[i + 1].equals(C) || word.equals(C) && (i - 1) >= 0 && words[i - 1] != null && words[i - 1].equals(VITAMIN);

            boolean YO_TEST_RESULT_PATTERN = word.equals(YO) && (i + 1) < words.length && words[i + 1] != null && words[i + 1].equals(TEST) && (i + 2) < words.length && words[i + 2] != null && words[i + 2].equals(RESULT) ||
                    word.equals(TEST) && (i - 1) >= 0 && words[i - 1] != null && words[i - 1].equals(YO) && (i + 1) < words.length && words[i + 1] != null && words[i + 1].equals(RESULT) ||
                    word.equals(RESULT) && (i - 1) >= 0 && words[i - 1] != null && words[i - 1].equals(TEST) && (i - 2) >= 0 && words[i - 2] != null && words[i - 2].equals(YO);

            boolean LINE_BREAKER_PATTERN = word.trim().equals(BR);

            ClickableString cs = new ClickableString(word, ClickableString.TYPE_NORMAL);
            if (DOES_NOT_PATTERN) {
                cs.setType(ClickableString.TYPE_DOES_NOT);
            } else if (READ_MORE_PATTERN) {
                cs.setType(ClickableString.TYPE_READ_MORE);
            } else if (VITAMIN_C_PATTERN) {
                cs.setType(ClickableString.TYPE_VITAMIN_C);
            } else if (YO_TEST_RESULT_PATTERN) {
                cs.setType(ClickableString.TYPE_YO_TEST_RESULT);
            } else if(LINE_BREAKER_PATTERN){
                cs.setType(TYPE_LINE_BREAKER);
            }else {
                cs.setType(ClickableString.TYPE_NORMAL);
            }
            clickableStringArrayList.add(cs);
        }

        int pressColor = ContextCompat.getColor(tv.getContext(), R.color.colorAccent);

        SimplifySpanBuild simplifySpanBuild = new SimplifySpanBuild();
        for (int i = 0; i < clickableStringArrayList.size(); i++) {
            ClickableString clickableString = clickableStringArrayList.get(i);
            switch (clickableString.getType()) {
                case ClickableString.TYPE_READ_MORE:
                    simplifySpanBuild.append(new SpecialTextUnit(clickableString.getWord().trim() + " ", Color.RED)
                            .setClickableUnit(new SpecialClickableUnit(tv, spanListener)
                                    .setPressBgColor(pressColor)
                                    .showUnderline()
                            )
                    );

                    break;

                case ClickableString.TYPE_DOES_NOT:
                case ClickableString.TYPE_YO_TEST_RESULT:
                    simplifySpanBuild.append(new SpecialTextUnit(clickableString.getWord() + " ", Color.BLACK)
                            .useTextBold()
                            .setClickableUnit(new SpecialClickableUnit(tv, spanListener)
                                    .setPressBgColor(pressColor)
                            )
                    );
                    break;
                case ClickableString.TYPE_VITAMIN_C:
                    simplifySpanBuild.append(new SpecialTextUnit(clickableString.getWord() + " ", Color.GREEN)
                            .setClickableUnit(new SpecialClickableUnit(tv, spanListener)
                                    .showUnderline()
                                    .setPressBgColor(pressColor)
                            )
                    );
                    break;
                case TYPE_LINE_BREAKER:
                    simplifySpanBuild.append(LINE_BREAKER);
                    break;
                case ClickableString.TYPE_NORMAL:
                default:
                    simplifySpanBuild.append(new SpecialTextUnit(clickableString.getWord().trim() + " ", Color.BLACK)
                    );
                    break;
            }
        }

        tv.setText(simplifySpanBuild.build());
    }

    public static boolean isReadMore(String text) {
        return text.equals(ClickableString.READ) || text.equals(ClickableString.MORE);
    }

    public static boolean isDoesNot(String text) {
        return text.equals(ClickableString.DOES) || text.equals(ClickableString.NOT);
    }

    public static boolean isVitaminC(String text) {
        return text.equals(ClickableString.VITAMIN) || text.equals(ClickableString.C);
    }

    @Override
    public String toString() {
        return "ClickableString{" +
                "word='" + word + '\'' +
                ", type=" + type +
                '}';
    }
}
