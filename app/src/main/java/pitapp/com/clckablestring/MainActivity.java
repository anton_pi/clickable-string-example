package pitapp.com.clckablestring;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import cn.iwgang.simplifyspan.customspan.CustomClickableSpan;
import cn.iwgang.simplifyspan.other.OnClickableSpanListener;

public class MainActivity extends AppCompatActivity implements OnClickableSpanListener {

    String str = "YO TEST RESULT The higher your MSC, the higher your YO SCORE. " +
            "However, your YO Score DOES NOT <br> <br> measure your ability <br> <br> to father a child as this depends on many fertility factors including your health and the health of your partner. " +
            "Read more...   " +
            "If you receive a LOW YO Score, remember that your YO TEST RESULTS are in the MODERATE/NORMAL range.  Vitamin C " +
            "Also remember that your YO SCORE is based on a study of thousands of men who have fathered children. " +
            "Think about the YO Score as being similar to comparing height, a person can be 5 feet tall or 6 feet tall, they are still NORMAL.  " +
            "Most men will be in the mid-range for height.  Your YO Score is the same, most men will be in the mid-range with a YO Score of 40 to 60. DOES NOT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tv = findViewById(R.id.tv);
        setClickable(tv, str);

    }

    private void setClickable(final TextView textView, final String text) {
        final String readMore = "Read more...";

        if (text.contains(readMore)) {
            Log.d(getClass().getSimpleName(), text);

            int readMoreStartIndex = text.indexOf(readMore);
            int readMoreEndIndex = readMoreStartIndex + readMore.length();

            String before = text.substring(0, readMoreStartIndex);
            String after = text.substring(readMoreEndIndex, text.length());
            String shortText = before + readMore;
            final String fullText = before + after;

            ClickableString.setClickableString(shortText, textView, new OnClickableSpanListener() {
                @Override
                public void onClick(TextView tv, CustomClickableSpan clickableSpan) {
                    String clickText = clickableSpan.getClickText();
                    if (ClickableString.isReadMore(clickText.trim())) {
                        Toast.makeText(tv.getContext(), "Read more", Toast.LENGTH_SHORT).show();
                        ClickableString.setClickableString(fullText, textView, this);
                    } else if (ClickableString.isDoesNot(clickText.trim())) {
                        Toast.makeText(tv.getContext(), "Does not", Toast.LENGTH_SHORT).show();
                    } else if (ClickableString.isVitaminC(clickText.trim())) {
                        Toast.makeText(tv.getContext(), "Vitamin C", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            ClickableString.setClickableString(text, textView, this);
        }
    }

    @Override
    public void onClick(TextView tv, CustomClickableSpan clickableSpan) {
        String clickText = clickableSpan.getClickText();
        if (ClickableString.isReadMore(clickText.trim())) {
            Toast.makeText(tv.getContext(), "Read more", Toast.LENGTH_SHORT).show();
        } else if (ClickableString.isDoesNot(clickText.trim())) {
            Toast.makeText(tv.getContext(), "Does not", Toast.LENGTH_SHORT).show();
        } else if (ClickableString.isVitaminC(clickText.trim())) {
            Toast.makeText(tv.getContext(), "Vitamin C", Toast.LENGTH_SHORT).show();
        }
    }
}
